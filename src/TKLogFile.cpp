
#pragma hdrstop

#include <System.IOUtils.hpp>
#include "TLangTranslator.h"
#include "TKLogFile.h"


#include <ctime>

#include <sys/stat.h>


//---------------------------------------------------------------------------

#pragma package(smart_init)

extern TLangTranslator T;

TKLogFile::TKLogFile(void){

	UnicodeString allFrames;
	DBGAllFrames = false;
	AlmID = -1;
	StateID = -1;
	xmlLogger = NULL;
	mtxFile = new TMutex(true);

	try{
		logPath = TPath::GetHomePath();
		logPath = logPath + "\\kerajet\\Log\\MegaCOM\\";
		logName = "kerajet.megaCOM.ALLShare.";

		logConfig = TPath::GetHomePath();
		logConfig = logConfig + "\\kerajet\\MegaCOM\\kerajet.megaCOM.logger.xml";

		if(!FileExists(logConfig)){
				throw EAbort("kerajet.megaCOM.logger.xml not found");
		}
		else{
			XMLDoc = new TXMLDocument(logConfig);
			xmlLogger = GetLogger(XMLDoc);
			allFrames = xmlLogger->Get_Config()->Get_FILE()->Get_DebugAllFrames();
			if(allFrames.LowerCase()=="yes")
				DBGAllFrames = true;
				AlmID = xmlLogger->Get_Config()->Get_FILE()->Get_AlarmFrameID();
				StateID = xmlLogger->Get_Config()->Get_FILE()->Get_StateFrameID();
		}
	}catch(Exception &e){
    	;
	}


}

TKLogFile::~TKLogFile(void){


}

// Get current date/time, format is YYYY-MM-DD.HH:mm:ss
AnsiString currentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%d/%m/%Y - %X", &tstruct);

	return buf;
}

long GetFileSize(UnicodeString filename)
{

	LARGE_INTEGER size;
	try{

		WIN32_FILE_ATTRIBUTE_DATA fad;
		if(filename=="")
			return -1;
		if(filename.Length()<=0)
			return -1;

		if (!GetFileAttributesEx(filename.w_str(), GetFileExInfoStandard, &fad))
			return -1; // error condition, could call GetLastError to find out more

		size.HighPart = fad.nFileSizeHigh;
		size.LowPart = fad.nFileSizeLow;

	}catch(Exception &e){
		return -1;
	}
	return size.QuadPart;


}

AnsiString strPadding(AnsiString msg, int max){

	std::string padstr;
	padstr = msg.c_str();
	if(padstr.length()<max)
		padstr+= std::string(max-padstr.length(),' ');
	msg = padstr.c_str();
	return msg;
}

void TKLogFile::Log(TPLCFrame *f){


	if(DBGAllFrames)
		LogValues(f);

	//Log ALARM FRAME
	if(f->idFrame==AlmID)
	  LogALM("PLC",f->getFrameDataValue(1),f->getFrameDataValue(2),f->getFrameDataValue(3),f->getFrameDataValue(4));
	else if(f->idFrame==StateID)
	  LogSTATE("PLC",f->getFrameDataValue(1));


}

void TKLogFile::LogValues(TPLCFrame *f){

	//Log all frames to file
	int dtCount = f->getFDCount();
	logLineFrame = "[";
	for(int i=1;i<dtCount;i++)
		logLineFrame+= AnsiString(f->getFrameDataValue(i))+",";

	logLineFrame.Delete(logLineFrame.Length(),1);
	//LOG ALL FRAME VALUES
	Log("PLC",f->idFrame,logLineFrame+"]");

}

void TKLogFile::InsertRecord(void){

//	_di_IXMLFieldType field;
//
//	//Get fields from record
//	for(int i=0;i<xmlRecord->Get_Field()->Count;i++){
//		field = xmlRecord->Get_Field()->Get_Item(i);
//		if(field->Get_Type()=="field"){
//			tempValues[i] = logFrame->getFrameDataValue(field->Get_fidx());
//		}
//	}
//
//	//Log ALARM FRAME
//	if(logFrame->idFrame==AlmID)
//	  LogALM("PLC",tempValues[0],tempValues[1],tempValues[2],tempValues[3]);
//	else if(logFrame->idFrame==StateID)
//	  LogSTATE("PLC",tempValues[0]);

}


void TKLogFile::Log(AnsiString applier, AnsiString msg){

	logLine = currentDateTime();
	logLine += strPadding(" ["+applier+"]",14);
	logLine += msg;

	AnsiString p = logPath+logName+applier+".log";
	AnsiString l = logLine;
	checkFile(p);
	writeFile(p,l);

}


void TKLogFile::Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc, AnsiString v){

	logLine = currentDateTime();
	logLine += strPadding(" ["+applier+"]",20);
	logLine += strPadding("["+src+"]",18);
	logLine += strPadding("["+dst+"]",10);
	logLine += strPadding("["+usr+"]",14);
//	logLine += strPadding("["+mid+"]",8);
//	logLine += " ["+mtype+"] ";
	logLine += desc+" = "+v;


	AnsiString p = logPath+logName+"execActions.log";
	AnsiString l = logLine;
	checkFile(p);
	writeFile(p,l);


}

void TKLogFile::Log(AnsiString applier, int almS, int almT, int almI, int almD){

	LogALM(applier, almS, almT, almI, almD);

}

void TKLogFile::writeFile(AnsiString filePath,AnsiString line){

	try{
		mtxFile->Acquire();
		pFile = fopen(filePath.c_str(), "a+");
		fprintf(pFile, "%s\n",line.c_str());
		fclose(pFile);
		mtxFile->Release();
	}catch(Exception &e){
		mtxFile->Release();
		AnsiString msg = e.Message;
	}

}

void TKLogFile::checkFile(AnsiString filePath){

	try{
		int maxSize = 2000*1024;
		AnsiString newName;
		AnsiString p;
		UnicodeString us = UnicodeString(filePath.c_str());
		long sz = GetFileSize(us);
		if(sz>maxSize){
			newName = TPath::GetFileNameWithoutExtension(us);
			p = TPath::GetDirectoryName(us);
			newName = p+"\\"+newName+".01.log";
			DeleteFile(newName.c_str());
			RenameFile(us,newName);
			DeleteFile(us);
		}
	}catch(Exception &e){
		AnsiString msg = e.Message;
	}

}


void TKLogFile::LogALM(AnsiString applier, int almS, int almT, int almI, int almD){

	logLine = currentDateTime();
	logLine += strPadding(" ["+applier+"]",14);
	logLine += "[ALM]  ";
	logLine += T.getTranslation("ALM_Severity"+AnsiString(almS))+",";
	logLine += T.getTranslation("ALM_Type"+AnsiString(almT))+",";
	logLine += T.getTranslation("ALM_Info"+AnsiString(almI));

	if(almD!=0)
		logLine += ","+T.getTranslation("ALM_Desc"+AnsiString(almD));

	if(almS==0 && almT==0 && almI==0 && almD==0)
		return;

	AnsiString p = logPath+logName+"machineSTATUS.log";
	AnsiString l = logLine;
	checkFile(p);
	writeFile(p,l);

	p = logPath+logName+"alarms.log";
	checkFile(p);
	writeFile(p,l);

}

void TKLogFile::LogSTATE(AnsiString applier, int state){

	logLine = currentDateTime();
	logLine += strPadding(" ["+applier+"]",14);
	logLine += "[INFO] ";
	logLine += T.getTranslation("STM"+AnsiString(state));

	AnsiString p = logPath+logName+"machineSTATUS.log";
	AnsiString l = logLine;
	checkFile(p);
	writeFile(p,l);


}


void TKLogFile::Log(AnsiString applier, int idFrame, AnsiString msg){

	logLine = currentDateTime();
	logLine += strPadding(" ["+applier+"]",8);
	logLine += msg;

	AnsiString p = logPath+logName+"PLC.frame"+AnsiString(idFrame)+".log";
	AnsiString l = logLine;
	checkFile(p);
	writeFile(p,l);

}



void TKLogFile::Log(AnsiString applier, int eventID){
	LogSTATE(applier,eventID);

}






