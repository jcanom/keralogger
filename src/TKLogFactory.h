#include "TKLogger.h"

#ifndef TKLogFactoryH

enum logger_type{LOG_FILE,LOG_WEV,LOG_DB,LOG_WS};
#define TKLogFactoryH
//---------------------------------------------------------------------------
//using namespace std;


class TKLogFactory{

	public:
		TKLogger *getLogger(logger_type logType);

};
#endif





