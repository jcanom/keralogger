//---------------------------------------------------------------------------
#include "TPLCFrame.h"
#include <queue>
#include <System.Classes.hpp>
#include "TKLogWrapper.h"
#ifndef KFrameLoggerH
#define KFrameLoggerH
//---------------------------------------------------------------------------
#endif

class ThFrameLogger;

class KFrameLogger
{

	private:
		TKLogWrapper *logSystem;
		ThFrameLogger *thFrameLog;

	public:
		KFrameLogger(TKLogWrapper *logSystem);
		~KFrameLogger();
		void Log(TPLCFrame *frame);

};


class ThFrameLogger : public TThread
{
	private:
		TPLCFrame *f_front;
		TPLCFrame *f_push;
		std::queue<TPLCFrame *> qFrame;
		bool stopAdd;
		TMutex *mtx;
   		TKLogWrapper *logSystem;
	protected:
		void __fastcall Execute();

	public:

		__fastcall ThFrameLogger(bool CreateSuspended,TKLogWrapper *lSystem);
		__fastcall ~ThFrameLogger(void);
		void addFrame(TPLCFrame *frame);
};