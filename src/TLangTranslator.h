//---------------------------------------------------------------------------

#ifndef TLangTranslatorH
#define TLangTranslatorH
//---------------------------------------------------------------------------
#include "siComp.h"


class TLangTranslator
{

	private:	// User declarations
		const TLangTranslator *siLangTranslator;
		TsiLang *lang;
		UnicodeString translTxt;


	public:		// User declarations
		TLangTranslator(void);
		~TLangTranslator(void);
		UnicodeString getTranslation(AnsiString id);
		void setLanguage(int langId);

};

#endif
