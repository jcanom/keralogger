//---------------------------------------------------------------------------

#pragma hdrstop
#include <System.IOUtils.hpp>
#include "TLangTranslator.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "siComp"


TLangTranslator T;

TLangTranslator::TLangTranslator(void){

	try{

		AnsiString sibPath = TPath::GetHomePath();
		sibPath = sibPath + "\\kerajet\\MegaCOM\\ASMessages.sib";

		TDataModule *dm = new TDataModule(0);
		lang = new TsiLang(dm);
		lang->DefaultLanguage = 3;
		lang->LoadAllFromBinaryFile(sibPath);
		lang->ActiveLanguage = 3;

	}catch(Exception &e){
		;
	}

}

TLangTranslator::~TLangTranslator(void){

	delete lang;

}

UnicodeString TLangTranslator::getTranslation(AnsiString id){

	translTxt = lang->GetTextOrDefault(id);
	if(translTxt==NULL)
		translTxt = "NOT FOUND";
	return translTxt;

}

void TLangTranslator::setLanguage(int id){

	lang->ActiveLanguage = id;


}
