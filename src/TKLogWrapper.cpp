//---------------------------------------------------------------------------

#pragma hdrstop
#include "TLangTranslator.h"
#include "TKLogWrapper.h"
#include <System.IOUtils.hpp>
#include <Xml.Win.msxmldom.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include "kerajetmegaCOMlogger.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)

extern TLangTranslator T;

TKLogWrapper::TKLogWrapper(void){

	try{

		loggerFILE = NULL;
		loggerDB = NULL;
		loggerWS = NULL;
		logEnabled = false;
		//Get log config data from XML
		UnicodeString confPath = TPath::GetHomePath();
		confPath = confPath + "\\kerajet\\MegaCOM\\kerajet.megaCOM.logger.xml";

		/*Create factory*/
		TKLogFactory *logFactory = new TKLogFactory();

		if(!FileExists(confPath))
			throw EAbort("kerajet.megaCOM.logger.xml not found");

		TXMLDocument *XMLDoc;
		XMLDoc = new TXMLDocument(confPath);
		_di_IXMLLoggerType 	xmlLogger = GetLogger(XMLDoc);
		UnicodeString logType = xmlLogger->Get_Config()->Get_Type();
		actionsToFilter = "";
		try{
			actionsToFilter = xmlLogger->Get_Config()->Get_ActionsFilter();
			actionsToFilter = ","+actionsToFilter+",";
		}catch(Exception &e){
			actionsToFilter = "";
		}

		logType = logType.LowerCase();
		/*Factory instantiating an object of type LOG_DB*/
		if(logType.Pos("db")>0 || logType.Pos("bd")>0)
			loggerDB = logFactory->getLogger(LOG_DB);
		/*Factory instantiating an object of type LOG_FILE*/
		if(logType.Pos("file")>0)
			loggerFILE = logFactory->getLogger(LOG_FILE);
		/*Factory instantiating an object of type LOG_WS*/
		if(logType.Pos("ws")>0)
			loggerWS = logFactory->getLogger(LOG_WS);

		 delete logFactory;

		 if(loggerFILE!=NULL || loggerWS!=NULL || loggerDB!=NULL)
			logEnabled = true;

	}catch(Exception &e){
		Log("ALLShare","logSystem: "+e.Message);
	}

}

TKLogWrapper::~TKLogWrapper(void){

	if(loggerFILE)
		delete loggerFILE;
	if(loggerDB)
		delete loggerDB;
	if(loggerWS)
		delete loggerWS;

	loggerFILE = NULL;
	loggerDB = NULL;
	loggerWS = NULL;

}


//PROCESS LOG
void TKLogWrapper::Log(AnsiString applier, AnsiString msg){

	try{
		if(loggerFILE)
			loggerFILE->Log(applier,msg);
	}catch(Exception &e){
		Log("ALLShare","logSystem - logging msg: "+e.Message);
	}

}

//FRAMES
void TKLogWrapper::Log(TPLCFrame *frame){

	try{
		if(loggerDB)
			loggerDB->Log(frame);
		if(loggerFILE)
			loggerFILE->Log(frame);
		if(loggerWS)
			loggerWS->Log(frame);
	}catch(Exception &e){
		Log("ALLShare","logSystem - logging frame: "+e.Message);
	}

}

//ACTIONS
void TKLogWrapper::Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc,AnsiString value){

	try{

		if(actionsToFilter.Pos(","+mid+",")!=0)
            return;

		if(loggerFILE)
			loggerFILE->Log(applier,src,dst,usr,mid,mtype,desc,value);
		if(loggerDB)
			loggerDB->Log(applier,src,dst,usr,mid,mtype,desc,value);
		if(loggerWS)
			loggerWS->Log(applier,src,dst,usr,mid,mtype,desc,value);

	}catch(Exception &e){
		Log("ALLShare","logSystem - logging action: "+e.Message);
	}

}

void TKLogWrapper::Log(AnsiString applier, int almS, int almT, int almI, int almD){

	try{
		if(loggerFILE)
			loggerFILE->Log(applier,almS,almT,almI,almD);
//		if(loggerDB)
//			loggerFILE->Log(applier,almS,almT,almI,almD);
	}catch(Exception &e){
		Log("ALLShare","logSystem - logging external alarm: "+e.Message);
	}

}

void TKLogWrapper::SetLanguage(int id){

	T.setLanguage(id);

}

//Log Events
void TKLogWrapper::Log(AnsiString applier, int eventID){

	try{
		if(loggerFILE)
			loggerFILE->Log(applier,eventID);
//		if(loggerDB)
//			loggerFILE->Log(applier,eventID);
	}catch(Exception &e){
		Log("ALLShare","logSystem - logging external event: "+e.Message);
	}

}

