//---------------------------------------------------------------------------


#pragma hdrstop

#include "TPLCFrame.h"
#include <time.h>


//---------------------------------------------------------------------------

#pragma package(smart_init)


__fastcall  TPLCFrame::TPLCFrame(int id) {

	lFData = new TList();
	xmlStream = new TMemoryStream();
	sendBuff = NULL;
	idFrame = id;
	fSize = 0;
	xmlSize = 0;
	canParse = false;
	rPeriod = 10; //msecs.
	leak = 1; //info can be leaked


}

__fastcall  TPLCFrame::~TPLCFrame(void) {

	TFrameData *f;
	for(int i=0;i<lFData->Count;i++){
		f = (TFrameData *)lFData->Items[i];
		delete f;
	}

	lFData->Clear();
	delete lFData;
    delete xmlStream;

}


void TPLCFrame::AddData(int fidx,int size,AnsiString pdesc,AnsiString pcode, int pbitNum){

	TFrameData *fD = new TFrameData();
	fD->idx = fidx;
	fD->bSize = size;
	fD->bit = pbitNum;
	fD->pcode = pcode;
	fD->value = -1;
	fD->desc = pdesc;
	//Data pointer ready to extract data from wholeframe
	fD->pRead = NULL;
	fSize+=size;
	lFData->Add((TFrameData *)fD);


}


void TPLCFrame::ExtractValues(void){


	int csize = 0;
	int lastIdx = 0;

	TFrameData *fd;
	TFrameData *fdPrev;

	//Scan all data list and move its data pointers.
	for(int i=0;i<lFData->Count;i++){

		fd = (TFrameData *)lFData->Items[i];

		//Move over rcvd. frame if needed
		//During bit access, stay within the same bytes.
		if(lastIdx!=fd->idx){
			lastIdx = fd->idx;
			csize+=fdPrev->bSize;
		}

		fd->value = 0;
		//Locate obj read pointer
		fd->pRead = &cFrame[csize];
		//Locate pointers position
		memcpy(&fd->value,fd->pRead,fd->bSize);
		//Bit access, get bit value
		if(fd->bit!=-1)
			fd->value = (fd->value & ( 1 << fd->bit )) >> fd->bit;

		fdPrev = fd;

	}

}

AnsiString TPLCFrame::ParseIT(byte *pFrame,AnsiString src_addr){


	TFrameData *fd;
	cFrame = pFrame;
	ExtractValues();

	//Build xml Frame to Observers
	AnsiString xmlLineParseIT;
	AnsiString aux;

	xmlLineParseIT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<PLC_Frame>\n\t<frameID>"+AnsiString(idFrame)+"</frameID>";
	xmlLineParseIT += "<leakable>"+AnsiString(leak)+"</leakable>\n\t<data>";
	//i = 1 --> Do not include Header in xml!

//	if(lFData==NULL)
//		bool lfdatacheck = true;

	try{
//		if(lFData->Count<0 || lFData->Count>500)
//			bool checkccc = true;
		if(lFData!=NULL){
			for(int i=1;i<lFData->Count;i++){
				fd = (TFrameData *)lFData->Items[i];
				if(fd!=NULL)
//					xmlLineParseIT += aux.sprintf(AnsiString("\t \t<%s>%d</%s>\n").c_str(),fd->desc,fd->value,fd->desc);
					xmlLineParseIT+= "\t\t<"+fd->desc+">"+AnsiString(fd->value)+"</"+fd->desc+">\n";
			}
		}
	}catch(System::Sysutils::Exception &e){
		;//int aaa=33;
	}


	xmlLineParseIT+= "\t</data>\n</PLC_Frame>";
	return xmlLineParseIT;

}



int TPLCFrame::getFrameDataValue(int idx){

	TFrameData *fd = (TFrameData *)lFData->Items[idx];
	return fd->value;

}

int TPLCFrame::getFDCount(void){

	return lFData->Count;

}

void TPLCFrame::copyFrom(TPLCFrame *f){

	if(f==NULL)
		return;

	TFrameData *fd;
	TFrameData *newfd;

    idFrame = f->idFrame;
	fSize = f->fSize;
	rPeriod = f->rPeriod;
	canParse = f->canParse;
	xmlSize = f->xmlSize;

	//Scan all data list and move its data pointers.
	for(int i=0;i<f->lFData->Count;i++){
		fd = (TFrameData *)f->lFData->Items[i];
		AddData(f->idFrame,fd->bSize,fd->desc,fd->pcode,fd->bit);
		newfd = (TFrameData *)lFData->Items[i];
		newfd->value = fd->value;
	}
}


//TPLCFrame *TPLCFrame::clone(void){
//
//
//	return new TPLCFrame(*this);
//
////	TFrameData *fd;
////	TFrameData *fdCopy;
////
////	//Scan all data list and move its data pointers.
////	for(int i=0;i<f->lFData->Count;i++){
////		fd = (TFrameData *)f->lFData->Items[i];
////		fdCopy = new TFrameData();
////
////		memcpy(fdCopy,fd,sizeof(TFrameData));
//////		fdCopy->idx = fd->idx;
//////		fdCopy->bSize = fd->bSize;
//////		fdCopy->bit = fd->bit;
//////		fdCopy->pcode = fd->pcode;
//////		fdCopy->value = -1;
//////		fdCopy->desc = fd->desc;
//////		//Data pointer ready to extract data from wholeframe
//////		fdCopy->pRead = NULL;
////
////		lFData->Add((TFrameData *)fdCopy);
////	}
//}

void TPLCFrame::setLeakable(int l){
	leak = l;
}
