//---------------------------------------------------------------------------

#include "TKLogFactory.h"
#include "TKLogger.h"
#include "TPLCFrame.h"


#ifndef TKLogWrapperH
#define TKLogWrapperH
//---------------------------------------------------------------------------


class TKLogWrapper{

	private:
		TKLogger *loggerFILE;
		TKLogger *loggerDB;
		TKLogger *loggerWS;
        AnsiString actionsToFilter;

	public:

		TKLogWrapper();
		~TKLogWrapper();
		void Log(TPLCFrame *f);
		void Log(AnsiString applier, AnsiString msg);
		void Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc,AnsiString value);
		void Log(AnsiString applier, int almS, int almT, int almI, int almD);
		void Log(AnsiString applier, int eventID);
		void SetLanguage(int id);
        bool logEnabled;

};
#endif
