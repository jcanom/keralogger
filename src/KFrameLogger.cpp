//---------------------------------------------------------------------------

#pragma hdrstop

#include "KFrameLogger.h"

#include <System.IOUtils.hpp>
//---------------------------------------------------------------------------
#pragma package(smart_init)


KFrameLogger::KFrameLogger(TKLogWrapper *lSystem){

	try{
		logSystem = lSystem;
		thFrameLog = new  ThFrameLogger(false,logSystem);
	}catch(Exception &e){
		logSystem->Log("ALLShare","KFrameLogger: "+e.Message);
	}

}

KFrameLogger::~KFrameLogger(void){

	thFrameLog->Terminate();


}



void KFrameLogger::Log(TPLCFrame *frame){

	try{
		if(logSystem!=NULL && logSystem->logEnabled){
			if(thFrameLog){
				if(frame!=NULL && frame->idFrame>=0 && frame->idFrame<=100){
					TPLCFrame * lFrame = new TPLCFrame(frame->idFrame);
					lFrame->copyFrom(frame);
					thFrameLog->addFrame(lFrame);
				}

			}
		}
	}catch(Exception &e){
		logSystem->Log("ALLShare","KFrameLogger: log Frame"+e.Message);
	}

}




__fastcall ThFrameLogger::ThFrameLogger(bool CreateSuspended,TKLogWrapper *lSystem)
	: TThread(CreateSuspended)
{
	logSystem = lSystem;
	mtx = new TMutex(true);
	stopAdd = false;
	//f_front = new TPLCFrame(0);
	FreeOnTerminate = true;
}


__fastcall ThFrameLogger::~ThFrameLogger(void){


	stopAdd = true;

//	mtx->Release();

	while(!qFrame.empty()>0){
		f_front = qFrame.front();
		qFrame.pop();
		delete f_front;
	}

	delete mtx;

}

//---------------------------------------------------------------------------
void __fastcall ThFrameLogger::Execute()
{
	NameThreadForDebugging(System::String(L"ThFrameLogger"));
	//---- Place thread code here ----

	while(!Terminated){
		try{
			mtx->Acquire();
			if(!qFrame.empty()>0){
				f_front = qFrame.front();
				//Log Frame!!!
				if(logSystem && f_front!=NULL && f_front->idFrame>=0 && f_front->idFrame<=100)
					logSystem->Log(f_front);

				qFrame.pop();
				delete f_front;

				mtx->Release();
			}else{
				mtx->Release();
				SleepEx(10,false);
			}
		}catch(Exception &e){
			AnsiString msg = e.Message;
		}
	}



}
//---------------------------------------------------------------------------

void ThFrameLogger::addFrame(TPLCFrame *frame){

//	if(!stopAdd){
//		mtx->Acquire();
//		if(frame!=NULL && frame->idFrame>=0 && frame->idFrame<=100){
//			f_push = new TPLCFrame(frame->idFrame);
//			//f_push = frame->clone();
//			f_push->copy(frame);
//			//memcpy(f_push,frame,sizeof(TPLCFrame));
//			qFrame.push(f_push);
//		}
//		mtx->Release();
//	}

	if(!stopAdd){
		mtx->Acquire();
		if(frame!=NULL && frame->idFrame>=0 && frame->idFrame<=100){
//			f_push = new TPLCFrame(frame->idFrame);
			//f_push = frame->clone();
//			f_push->copy(frame);
			//memcpy(f_push,frame,sizeof(TPLCFrame));
			qFrame.push(frame);
		}
		mtx->Release();
	}

}