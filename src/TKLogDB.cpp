
#pragma hdrstop

#include <System.IOUtils.hpp>
#include "TKLogDB.h"
#include <algorithm>    // std::for_each
#include <vector>       // std::vector



#include <ctime>

#include <sys/stat.h>


//---------------------------------------------------------------------------

#pragma package(smart_init)


TKLogDB::TKLogDB(void){

	logConfig = TPath::GetHomePath();
	logConfig = logConfig + "\\kerajet\\MegaCOM\\kerajet.megaCOM.logger.xml";

	if(!FileExists(logConfig))
			throw EAbort("kerajet.megaCOM.logger.xml not found");

	XMLDoc = new TXMLDocument(logConfig);

	xmlLogger = GetLogger(XMLDoc);
	Connect();

}

void TKLogDB::connErrors(void){

}

void TKLogDB::Connect(void){

	Connection = new TADOConnection(0);
	Connection->LoginPrompt = false;
	Connection->ConnectionString = xmlLogger->Get_Config()->Get_DB()->Get_ConnectionString();
	Connection->Connected = true;


	Table = new TADOTable(0);
	Table->Connection = Connection;
	Table->AutoCalcFields = true;

}

TKLogDB::~TKLogDB(void){

   Connection->Close();
   delete Connection;

}

void TKLogDB::Log(TPLCFrame *f){

	logFrame = f;
	//Search Frame id
	for(int i=0;i<xmlLogger->Get_FramesLog()->Count;i++){
		if(f->idFrame == xmlLogger->Get_FramesLog()->Get_Frame(i)->Get_Id()){
			//Get all records to insert
			for(int j=0;j<xmlLogger->Get_FramesLog()->Get_Frame(i)->Count;j++){
			  xmlRecord = xmlLogger->Get_FramesLog()->Get_Frame(i)->Get_Record(j);
			  InsertRecord();
			}
		}

	}

}


void TKLogDB::Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc, AnsiString v){


	if(!Table->Connection->Connected)
		return;

	//Insert Record in table
	Table->Active = false;
	Table->TableName = "ExecutedActions";
	Table->Active = true;
	Table->Append();

	Table->Fields->FieldByName("Application")->Text = applier;
	Table->Fields->FieldByName("MachineID")->Text = xmlLogger->Get_Config()->Get_MachineID();
	Table->Fields->FieldByName("SourceAddress")->Text = src;
	Table->Fields->FieldByName("DestinationAddress")->Text = dst;
	Table->Fields->FieldByName("ExecutedByUser")->Text = usr;
	Table->Fields->FieldByName("ActionCode")->Text = mtype;
	Table->Fields->FieldByName("ActionType")->Text = "W";
	Table->Fields->FieldByName("Value")->Text = v;
	Table->Fields->FieldByName("Description")->Text = desc;

	Table->Post();
	Table->Active = false;


}
void TKLogDB::InsertRecord(void){

	_di_IXMLFieldType field;
	int v;


	if(!Table->Connection->Connected)
		return;

	//Insert Record in table
	Table->Active = false;
	Table->TableName = xmlRecord->Get_Table();
	Table->Active = true;
	Table->Append();

	//Get fields from record
	for(int i=0;i<xmlRecord->Get_Field()->Count;i++){
		field = xmlRecord->Get_Field()->Get_Item(i);
		if(field->Get_Type()=="field"){
			v = logFrame->getFrameDataValue(field->Get_fidx());
			Table->Fields->FieldByName(field->Get_Field())->Text = v;
		}else if(field->Get_Type()=="config"){
			Table->Fields->FieldByName(field->Get_Field())->Text = xmlLogger->Get_Config()->ChildNodes->FindNode(field->Get_Field())->Text;
		}else if(field->Get_Type()=="value"){
			Table->Fields->FieldByName(field->Get_Field())->Text = 	field->Get_Value();
		}
	}

	Table->Post();
	Table->Active = false;

}




