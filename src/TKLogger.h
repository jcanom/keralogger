#include <vcl.h>
#include "TLangTranslator.h"
#include "TPLCFrame.h"

#ifndef TKLoggerH
#define TKLoggerH
//---------------------------------------------------------------------------

// Abstract Base Class
class TKLogger{

	public:
		virtual ~TKLogger(){};
		virtual void Log(TPLCFrame *frame){};
		virtual void Log(AnsiString applier, AnsiString msg){};
		virtual void Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc, AnsiString v){};
		virtual void Log(AnsiString applier, int almS, int almT, int almI, int almD){};
		virtual void Log(AnsiString applier, int eventID){};
		virtual void SetTranslator(TLangTranslator *T){};
};

#endif
