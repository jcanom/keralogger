//---------------------------------------------------------------------------
#include <vcl.h>
#include "TKLogger.h"
#include <System.Classes.hpp>
#include "TPLCFrame.h"
#include "kerajetmegaCOMlogger.h"
#include <Xml.Win.msxmldom.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>;
#include <stdio.h>
#include <string>
#include <queue>

#ifndef TKLogDBH
#define TKLogDBH
//---------------------------------------------------------------------------




class TKLogDB: public TKLogger{

	private:

		AnsiString logConfig;

		TXMLDocument *XMLDoc;
		_di_IXMLLoggerType 	xmlLogger;
		_di_IXMLRecordType 	xmlRecord;
		void InsertRecord(void);
		TPLCFrame *logFrame;
		void Connect();
		TADOConnection *Connection;
		TADOTable *Table;
		void connErrors();

	public:
		TKLogDB();
		~TKLogDB();
		void Log(TPLCFrame *frame);
		void Log(AnsiString applier, AnsiString msg){};
		void Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc, AnsiString v);
		void Log(AnsiString applier, int almS, int almT, int almI, int almD){};
		void Log(AnsiString applier, int eventID){};


};

//---------------------------------------------------------------------------
#endif



