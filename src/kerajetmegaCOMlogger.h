
// *************************************************************************************************** //
//                                                                                                   
//                                         XML Data Binding                                          
//                                                                                                   
//         Generated on: 26/08/2016 18:28:19                                                         
//       Generated from: C:\Users\jcano\AppData\Roaming\kerajet\MegaCOM\kerajet.megaCOM.logger.xml   
//   Settings stored in: C:\Users\jcano\AppData\Roaming\kerajet\MegaCOM\kerajet.megaCOM.logger.xdb   
//                                                                                                   
// *************************************************************************************************** //

#ifndef   kerajetmegaCOMloggerH
#define   kerajetmegaCOMloggerH

#include <System.hpp>
#include <Xml.Xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <Xml.XMLDoc.hpp>
#include <XMLNodeImp.h>


// Forward Decls 

__interface IXMLLoggerType;
typedef System::DelphiInterface<IXMLLoggerType> _di_IXMLLoggerType;
__interface IXMLConfigType;
typedef System::DelphiInterface<IXMLConfigType> _di_IXMLConfigType;
__interface IXMLDBType;
typedef System::DelphiInterface<IXMLDBType> _di_IXMLDBType;
__interface IXMLFILEType;
typedef System::DelphiInterface<IXMLFILEType> _di_IXMLFILEType;
__interface IXMLFramesLogType;
typedef System::DelphiInterface<IXMLFramesLogType> _di_IXMLFramesLogType;
__interface IXMLFrameType;
typedef System::DelphiInterface<IXMLFrameType> _di_IXMLFrameType;
__interface IXMLRecordType;
typedef System::DelphiInterface<IXMLRecordType> _di_IXMLRecordType;
__interface IXMLFieldType;
typedef System::DelphiInterface<IXMLFieldType> _di_IXMLFieldType;
__interface IXMLFieldTypeList;
typedef System::DelphiInterface<IXMLFieldTypeList> _di_IXMLFieldTypeList;

// IXMLLoggerType 

__interface INTERFACE_UUID("{AB7F4BBB-2457-4C64-87B3-C2F75122D48C}") IXMLLoggerType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual _di_IXMLConfigType __fastcall Get_Config() = 0;
  virtual _di_IXMLFramesLogType __fastcall Get_FramesLog() = 0;
  // Methods & Properties 
  __property _di_IXMLConfigType Config = { read=Get_Config };
  __property _di_IXMLFramesLogType FramesLog = { read=Get_FramesLog };
};

// IXMLConfigType 

__interface INTERFACE_UUID("{EA1BB6B3-E0F7-431F-972F-6E598E265C43}") IXMLConfigType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_Type() = 0;
  virtual int __fastcall Get_MachineID() = 0;
  virtual System::UnicodeString __fastcall Get_ActionsFilter() = 0;
  virtual _di_IXMLDBType __fastcall Get_DB() = 0;
  virtual _di_IXMLFILEType __fastcall Get_FILE() = 0;
  virtual void __fastcall Set_Type(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_MachineID(int Value) = 0;
  virtual void __fastcall Set_ActionsFilter(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString Type = { read=Get_Type, write=Set_Type };
  __property int MachineID = { read=Get_MachineID, write=Set_MachineID };
  __property System::UnicodeString ActionsFilter = { read=Get_ActionsFilter, write=Set_ActionsFilter };
  __property _di_IXMLDBType DB = { read=Get_DB };
  __property _di_IXMLFILEType FILE = { read=Get_FILE };
};

// IXMLDBType 

__interface INTERFACE_UUID("{C47AEC92-9233-49E5-B3E5-1EF1B283BB20}") IXMLDBType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_ConnectionString() = 0;
  virtual System::UnicodeString __fastcall Get_Password() = 0;
  virtual System::UnicodeString __fastcall Get_User() = 0;
  virtual void __fastcall Set_ConnectionString(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_Password(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_User(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString ConnectionString = { read=Get_ConnectionString, write=Set_ConnectionString };
  __property System::UnicodeString Password = { read=Get_Password, write=Set_Password };
  __property System::UnicodeString User = { read=Get_User, write=Set_User };
};

// IXMLFILEType 

__interface INTERFACE_UUID("{7728981D-7DAF-4D94-B1B3-E9C999D80BE6}") IXMLFILEType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_DebugAllFrames() = 0;
  virtual int __fastcall Get_AlarmFrameID() = 0;
  virtual int __fastcall Get_StateFrameID() = 0;
  virtual void __fastcall Set_DebugAllFrames(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_AlarmFrameID(int Value) = 0;
  virtual void __fastcall Set_StateFrameID(int Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString DebugAllFrames = { read=Get_DebugAllFrames, write=Set_DebugAllFrames };
  __property int AlarmFrameID = { read=Get_AlarmFrameID, write=Set_AlarmFrameID };
  __property int StateFrameID = { read=Get_StateFrameID, write=Set_StateFrameID };
};

// IXMLFramesLogType 

__interface INTERFACE_UUID("{E9B14F43-F159-43A4-AF4A-F0552A09784B}") IXMLFramesLogType : public Xml::Xmlintf::IXMLNodeCollection
{
public:
public:
  // Property Accessors 
  virtual _di_IXMLFrameType __fastcall Get_Frame(int Index) = 0;
  // Methods & Properties 
  virtual _di_IXMLFrameType __fastcall Add() = 0;
  virtual _di_IXMLFrameType __fastcall Insert(const int Index) = 0;
  __property _di_IXMLFrameType Frame[int Index] = { read=Get_Frame };/* default */
};

// IXMLFrameType 

__interface INTERFACE_UUID("{395A3321-A47F-4167-82C5-AB2373C6D998}") IXMLFrameType : public Xml::Xmlintf::IXMLNodeCollection
{
public:
public:
  // Property Accessors 
  virtual int __fastcall Get_Id() = 0;
  virtual _di_IXMLRecordType __fastcall Get_Record(int Index) = 0;
  virtual void __fastcall Set_Id(int Value) = 0;
  // Methods & Properties 
  virtual _di_IXMLRecordType __fastcall Add() = 0;
  virtual _di_IXMLRecordType __fastcall Insert(const int Index) = 0;
  __property int Id = { read=Get_Id, write=Set_Id };
  __property _di_IXMLRecordType Record[int Index] = { read=Get_Record };/* default */
};

// IXMLRecordType 

__interface INTERFACE_UUID("{0162FEC4-E136-402A-BCD0-8C26E54C3C80}") IXMLRecordType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_Table() = 0;
  virtual _di_IXMLFieldTypeList __fastcall Get_Field() = 0;
  virtual void __fastcall Set_Table(System::UnicodeString Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString Table = { read=Get_Table, write=Set_Table };
  __property _di_IXMLFieldTypeList Field = { read=Get_Field };
};

// IXMLFieldType 

__interface INTERFACE_UUID("{C8EDDEEB-DC70-4064-A495-51F2CC3FB71E}") IXMLFieldType : public Xml::Xmlintf::IXMLNode
{
public:
  // Property Accessors 
  virtual System::UnicodeString __fastcall Get_Type() = 0;
  virtual System::UnicodeString __fastcall Get_Field() = 0;
  virtual int __fastcall Get_fidx() = 0;
  virtual int __fastcall Get_Value() = 0;
  virtual void __fastcall Set_Type(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_Field(System::UnicodeString Value) = 0;
  virtual void __fastcall Set_fidx(int Value) = 0;
  virtual void __fastcall Set_Value(int Value) = 0;
  // Methods & Properties 
  __property System::UnicodeString Type = { read=Get_Type, write=Set_Type };
  __property System::UnicodeString Field = { read=Get_Field, write=Set_Field };
  __property int fidx = { read=Get_fidx, write=Set_fidx };
  __property int Value = { read=Get_Value, write=Set_Value };
};

// IXMLFieldTypeList 

__interface INTERFACE_UUID("{C464A0F7-136F-45A5-9DB9-3CBC1598F5BF}") IXMLFieldTypeList : public Xml::Xmlintf::IXMLNodeCollection
{
public:
  // Methods & Properties 
  virtual _di_IXMLFieldType __fastcall Add() = 0;
  virtual _di_IXMLFieldType __fastcall Insert(const int Index) = 0;

  virtual _di_IXMLFieldType __fastcall Get_Item(int Index) = 0;
  __property _di_IXMLFieldType Items[int Index] = { read=Get_Item /* default */ };
};

// Forward Decls 

class TXMLLoggerType;
class TXMLConfigType;
class TXMLDBType;
class TXMLFILEType;
class TXMLFramesLogType;
class TXMLFrameType;
class TXMLRecordType;
class TXMLFieldType;
class TXMLFieldTypeList;

// TXMLLoggerType 

class TXMLLoggerType : public Xml::Xmldoc::TXMLNode, public IXMLLoggerType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLLoggerType 
  virtual _di_IXMLConfigType __fastcall Get_Config();
  virtual _di_IXMLFramesLogType __fastcall Get_FramesLog();
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLConfigType 

class TXMLConfigType : public Xml::Xmldoc::TXMLNode, public IXMLConfigType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLConfigType 
  virtual System::UnicodeString __fastcall Get_Type();
  virtual int __fastcall Get_MachineID();
  virtual System::UnicodeString __fastcall Get_ActionsFilter();
  virtual _di_IXMLDBType __fastcall Get_DB();
  virtual _di_IXMLFILEType __fastcall Get_FILE();
  virtual void __fastcall Set_Type(System::UnicodeString Value);
  virtual void __fastcall Set_MachineID(int Value);
  virtual void __fastcall Set_ActionsFilter(System::UnicodeString Value);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLDBType 

class TXMLDBType : public Xml::Xmldoc::TXMLNode, public IXMLDBType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLDBType 
  virtual System::UnicodeString __fastcall Get_ConnectionString();
  virtual System::UnicodeString __fastcall Get_Password();
  virtual System::UnicodeString __fastcall Get_User();
  virtual void __fastcall Set_ConnectionString(System::UnicodeString Value);
  virtual void __fastcall Set_Password(System::UnicodeString Value);
  virtual void __fastcall Set_User(System::UnicodeString Value);
};

// TXMLFILEType 

class TXMLFILEType : public Xml::Xmldoc::TXMLNode, public IXMLFILEType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLFILEType 
  virtual System::UnicodeString __fastcall Get_DebugAllFrames();
  virtual int __fastcall Get_AlarmFrameID();
  virtual int __fastcall Get_StateFrameID();
  virtual void __fastcall Set_DebugAllFrames(System::UnicodeString Value);
  virtual void __fastcall Set_AlarmFrameID(int Value);
  virtual void __fastcall Set_StateFrameID(int Value);
};

// TXMLFramesLogType 

class TXMLFramesLogType : public Xml::Xmldoc::TXMLNodeCollection, public IXMLFramesLogType
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLFramesLogType 
  virtual _di_IXMLFrameType __fastcall Get_Frame(int Index);
  virtual _di_IXMLFrameType __fastcall Add();
  virtual _di_IXMLFrameType __fastcall Insert(const int Index);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLFrameType 

class TXMLFrameType : public Xml::Xmldoc::TXMLNodeCollection, public IXMLFrameType
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLFrameType 
  virtual int __fastcall Get_Id();
  virtual _di_IXMLRecordType __fastcall Get_Record(int Index);
  virtual void __fastcall Set_Id(int Value);
  virtual _di_IXMLRecordType __fastcall Add();
  virtual _di_IXMLRecordType __fastcall Insert(const int Index);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLRecordType 

class TXMLRecordType : public Xml::Xmldoc::TXMLNode, public IXMLRecordType
{
  __IXMLNODE_IMPL__
private:
  _di_IXMLFieldTypeList FField;
protected:
  // IXMLRecordType 
  virtual System::UnicodeString __fastcall Get_Table();
  virtual _di_IXMLFieldTypeList __fastcall Get_Field();
  virtual void __fastcall Set_Table(System::UnicodeString Value);
public:
  virtual void __fastcall AfterConstruction(void);
};

// TXMLFieldType 

class TXMLFieldType : public Xml::Xmldoc::TXMLNode, public IXMLFieldType
{
  __IXMLNODE_IMPL__
protected:
  // IXMLFieldType 
  virtual System::UnicodeString __fastcall Get_Type();
  virtual System::UnicodeString __fastcall Get_Field();
  virtual int __fastcall Get_fidx();
  virtual int __fastcall Get_Value();
  virtual void __fastcall Set_Type(System::UnicodeString Value);
  virtual void __fastcall Set_Field(System::UnicodeString Value);
  virtual void __fastcall Set_fidx(int Value);
  virtual void __fastcall Set_Value(int Value);
};

// TXMLFieldTypeList 

class TXMLFieldTypeList : public Xml::Xmldoc::TXMLNodeCollection, public IXMLFieldTypeList
{
  __IXMLNODECOLLECTION_IMPL__
protected:
  // IXMLFieldTypeList 
  virtual _di_IXMLFieldType __fastcall Add();
  virtual _di_IXMLFieldType __fastcall Insert(const int Index);

  virtual _di_IXMLFieldType __fastcall Get_Item(int Index);
};

// Global Functions 

_di_IXMLLoggerType __fastcall GetLogger(Xml::Xmlintf::_di_IXMLDocument Doc);
_di_IXMLLoggerType __fastcall GetLogger(Xml::Xmldoc::TXMLDocument *Doc);
_di_IXMLLoggerType __fastcall LoadLogger(const System::UnicodeString& FileName);
_di_IXMLLoggerType __fastcall  NewLogger();

#define TargetNamespace ""

#endif