//---------------------------------------------------------------------------
#include <vcl.h>
#include "TKLogger.h"
#include <System.Classes.hpp>
#include "kerajetmegaCOMlogger.h"
#include <Xml.Win.msxmldom.hpp>
#include <Xml.XMLDoc.hpp>
#include <Xml.xmldom.hpp>
#include <Xml.XMLIntf.hpp>
#include <stdio.h>
#include <string>
#include <queue>
#include "TPLCFrame.h"

#ifndef TKLogFileH
#define TKLogFileH
//---------------------------------------------------------------------------


class TKLogFile: public TKLogger{

	private:

		TXMLDocument *XMLDoc;
		_di_IXMLLoggerType 	xmlLogger;
		_di_IXMLRecordType 	xmlRecord;
		void InsertRecord(void);

		TPLCFrame *logFrame;


		AnsiString logConfig;
		AnsiString logPath;
		AnsiString logName;
		AnsiString logLine;
		AnsiString logLineFrame;
//		AnsiString cLine;
//		AnsiString cPath;
		int vl;

		bool DBGAllFrames;
		int AlmID,StateID;
		void writeFile(AnsiString filePath,AnsiString line);
		void checkFile(AnsiString filePath);
		FILE* pFile;
		TMutex *mtxFile;


		int tempValues[50];

		//Log Frame Values
		void LogValues(TPLCFrame *frame);
		void LogSTATE(AnsiString applier, int id);
		void LogALM(AnsiString applier, int almS, int almT, int almI, int almD);

		void Log(AnsiString applier, int frame, AnsiString msg);


	public:
		TKLogFile();
		~TKLogFile();
		//LOG FRAME
		void Log(TPLCFrame *frame);
		//LOG MESSAGE
		void Log(AnsiString applier, AnsiString msg);
		//LOG ACTION
		void Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc, AnsiString v);
//       	void Log(AnsiString applier, int frame, AnsiString fieldID, AnsiString fieldDesc, AnsiString v);
		//LOG ALARM
		void Log(AnsiString applier, int almS, int almT, int almI, int almD);
		//LOG EVENT
		void Log(AnsiString applier, int eventID);


};


#endif



