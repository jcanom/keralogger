
// *************************************************************************************************** //
//                                                                                                   
//                                         XML Data Binding                                          
//                                                                                                   
//         Generated on: 26/08/2016 18:28:19                                                         
//       Generated from: C:\Users\jcano\AppData\Roaming\kerajet\MegaCOM\kerajet.megaCOM.logger.xml   
//   Settings stored in: C:\Users\jcano\AppData\Roaming\kerajet\MegaCOM\kerajet.megaCOM.logger.xdb   
//                                                                                                   
// *************************************************************************************************** //

#include <System.hpp>
#pragma hdrstop

#include "kerajetmegaCOMlogger.h"


// Global Functions 

_di_IXMLLoggerType __fastcall GetLogger(Xml::Xmlintf::_di_IXMLDocument Doc)
{
  return (_di_IXMLLoggerType) Doc->GetDocBinding("Logger", __classid(TXMLLoggerType), TargetNamespace);
};

_di_IXMLLoggerType __fastcall GetLogger(Xml::Xmldoc::TXMLDocument *Doc)
{
  Xml::Xmlintf::_di_IXMLDocument DocIntf;
  Doc->GetInterface(DocIntf);
  return GetLogger(DocIntf);
};

_di_IXMLLoggerType __fastcall LoadLogger(const System::UnicodeString& FileName)
{
  return (_di_IXMLLoggerType) Xml::Xmldoc::LoadXMLDocument(FileName)->GetDocBinding("Logger", __classid(TXMLLoggerType), TargetNamespace);
};

_di_IXMLLoggerType __fastcall  NewLogger()
{
  return (_di_IXMLLoggerType) Xml::Xmldoc::NewXMLDocument()->GetDocBinding("Logger", __classid(TXMLLoggerType), TargetNamespace);
};

// TXMLLoggerType 

void __fastcall TXMLLoggerType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("Config"), __classid(TXMLConfigType));
  RegisterChildNode(System::UnicodeString("FramesLog"), __classid(TXMLFramesLogType));
  Xml::Xmldoc::TXMLNode::AfterConstruction();
};

_di_IXMLConfigType __fastcall TXMLLoggerType::Get_Config()
{
  return (_di_IXMLConfigType) GetChildNodes()->Nodes[System::UnicodeString("Config")];
};

_di_IXMLFramesLogType __fastcall TXMLLoggerType::Get_FramesLog()
{
  return (_di_IXMLFramesLogType) GetChildNodes()->Nodes[System::UnicodeString("FramesLog")];
};

// TXMLConfigType 

void __fastcall TXMLConfigType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("DB"), __classid(TXMLDBType));
  RegisterChildNode(System::UnicodeString("FILE"), __classid(TXMLFILEType));
  Xml::Xmldoc::TXMLNode::AfterConstruction();
};

System::UnicodeString __fastcall TXMLConfigType::Get_Type()
{
  return GetChildNodes()->Nodes[System::UnicodeString("Type")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLConfigType::Set_Type(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("Type")]->NodeValue = Value;
};

int __fastcall TXMLConfigType::Get_MachineID()
{
  return GetChildNodes()->Nodes[System::UnicodeString("MachineID")]->NodeValue.operator int();
};

void __fastcall TXMLConfigType::Set_MachineID(int Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("MachineID")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLConfigType::Get_ActionsFilter()
{
  return GetChildNodes()->Nodes[System::UnicodeString("ActionsFilter")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLConfigType::Set_ActionsFilter(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("ActionsFilter")]->NodeValue = Value;
};

_di_IXMLDBType __fastcall TXMLConfigType::Get_DB()
{
  return (_di_IXMLDBType) GetChildNodes()->Nodes[System::UnicodeString("DB")];
};

_di_IXMLFILEType __fastcall TXMLConfigType::Get_FILE()
{
  return (_di_IXMLFILEType) GetChildNodes()->Nodes[System::UnicodeString("FILE")];
};

// TXMLDBType 

System::UnicodeString __fastcall TXMLDBType::Get_ConnectionString()
{
  return GetChildNodes()->Nodes[System::UnicodeString("ConnectionString")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLDBType::Set_ConnectionString(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("ConnectionString")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLDBType::Get_Password()
{
  return GetChildNodes()->Nodes[System::UnicodeString("Password")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLDBType::Set_Password(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("Password")]->NodeValue = Value;
};

System::UnicodeString __fastcall TXMLDBType::Get_User()
{
  return GetChildNodes()->Nodes[System::UnicodeString("User")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLDBType::Set_User(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("User")]->NodeValue = Value;
};

// TXMLFILEType 

System::UnicodeString __fastcall TXMLFILEType::Get_DebugAllFrames()
{
  return GetChildNodes()->Nodes[System::UnicodeString("DebugAllFrames")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLFILEType::Set_DebugAllFrames(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("DebugAllFrames")]->NodeValue = Value;
};

int __fastcall TXMLFILEType::Get_AlarmFrameID()
{
  return GetChildNodes()->Nodes[System::UnicodeString("AlarmFrameID")]->NodeValue.operator int();
};

void __fastcall TXMLFILEType::Set_AlarmFrameID(int Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("AlarmFrameID")]->NodeValue = Value;
};

int __fastcall TXMLFILEType::Get_StateFrameID()
{
  return GetChildNodes()->Nodes[System::UnicodeString("StateFrameID")]->NodeValue.operator int();
};

void __fastcall TXMLFILEType::Set_StateFrameID(int Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("StateFrameID")]->NodeValue = Value;
};

// TXMLFramesLogType 

void __fastcall TXMLFramesLogType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("Frame"), __classid(TXMLFrameType));
  ItemTag = "Frame";
  ItemInterface = __uuidof(IXMLFrameType);
  Xml::Xmldoc::TXMLNodeCollection::AfterConstruction();
};

_di_IXMLFrameType __fastcall TXMLFramesLogType::Get_Frame(int Index)
{
  return (_di_IXMLFrameType) List->Nodes[Index];
};

_di_IXMLFrameType __fastcall TXMLFramesLogType::Add()
{
  return (_di_IXMLFrameType) AddItem(-1);
};

_di_IXMLFrameType __fastcall TXMLFramesLogType::Insert(const int Index)
{
  return (_di_IXMLFrameType) AddItem(Index);
};

// TXMLFrameType 

void __fastcall TXMLFrameType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("Record"), __classid(TXMLRecordType));
  ItemTag = "Record";
  ItemInterface = __uuidof(IXMLRecordType);
  Xml::Xmldoc::TXMLNodeCollection::AfterConstruction();
};

int __fastcall TXMLFrameType::Get_Id()
{
  return GetAttributeNodes()->Nodes[System::UnicodeString("Id")]->NodeValue.operator int();
};

void __fastcall TXMLFrameType::Set_Id(int Value)
{
  SetAttribute(System::UnicodeString("Id"), Value);
};

_di_IXMLRecordType __fastcall TXMLFrameType::Get_Record(int Index)
{
  return (_di_IXMLRecordType) List->Nodes[Index];
};

_di_IXMLRecordType __fastcall TXMLFrameType::Add()
{
  return (_di_IXMLRecordType) AddItem(-1);
};

_di_IXMLRecordType __fastcall TXMLFrameType::Insert(const int Index)
{
  return (_di_IXMLRecordType) AddItem(Index);
};

// TXMLRecordType 

void __fastcall TXMLRecordType::AfterConstruction(void)
{
  RegisterChildNode(System::UnicodeString("Field"), __classid(TXMLFieldType));
  CollectionCreater<_di_IXMLFieldTypeList, TXMLFieldTypeList, IXMLFieldType>::DoCreate(this, FField, "Field");
  Xml::Xmldoc::TXMLNode::AfterConstruction();
};

System::UnicodeString __fastcall TXMLRecordType::Get_Table()
{
  return GetChildNodes()->Nodes[System::UnicodeString("Table")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLRecordType::Set_Table(System::UnicodeString Value)
{
  GetChildNodes()->Nodes[System::UnicodeString("Table")]->NodeValue = Value;
};

_di_IXMLFieldTypeList __fastcall TXMLRecordType::Get_Field()
{
  return (_di_IXMLFieldTypeList) FField;
};

// TXMLFieldType 

System::UnicodeString __fastcall TXMLFieldType::Get_Type()
{
  return GetAttributeNodes()->Nodes[System::UnicodeString("Type")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLFieldType::Set_Type(System::UnicodeString Value)
{
  SetAttribute(System::UnicodeString("Type"), Value);
};

System::UnicodeString __fastcall TXMLFieldType::Get_Field()
{
  return GetAttributeNodes()->Nodes[System::UnicodeString("Field")]->NodeValue.operator System::UnicodeString();
};

void __fastcall TXMLFieldType::Set_Field(System::UnicodeString Value)
{
  SetAttribute(System::UnicodeString("Field"), Value);
};

int __fastcall TXMLFieldType::Get_fidx()
{
  return GetAttributeNodes()->Nodes[System::UnicodeString("fidx")]->NodeValue.operator int();
};

void __fastcall TXMLFieldType::Set_fidx(int Value)
{
  SetAttribute(System::UnicodeString("fidx"), Value);
};

int __fastcall TXMLFieldType::Get_Value()
{
  return GetAttributeNodes()->Nodes[System::UnicodeString("Value")]->NodeValue.operator int();
};

void __fastcall TXMLFieldType::Set_Value(int Value)
{
  SetAttribute(System::UnicodeString("Value"), Value);
};

// TXMLFieldTypeList 

_di_IXMLFieldType __fastcall TXMLFieldTypeList::Add()
{
  return (_di_IXMLFieldType) AddItem(-1);
};

_di_IXMLFieldType __fastcall TXMLFieldTypeList::Insert(const int Index)
{
  return (_di_IXMLFieldType) AddItem(Index);
};

_di_IXMLFieldType __fastcall TXMLFieldTypeList::Get_Item(int Index)
{
  return (_di_IXMLFieldType) List->Nodes[Index];
};
