
#pragma hdrstop

#include <System.IOUtils.hpp>
#include "TKLogWS.h"
#include <algorithm>    // std::for_each
#include <vector>       // std::vector




#include <ctime>

#include <sys/stat.h>


//---------------------------------------------------------------------------

#pragma package(smart_init)


TKLogWS::TKLogWS(void){

	Connection = NULL;
	logConfig = TPath::GetHomePath();
	logConfig = logConfig + "\\kerajet\\MegaCOM\\kerajet.megaCOM.logger.xml";

	if(!FileExists(logConfig))
			throw EAbort("kerajet.megaCOM.logger.xml not found");

	XMLDoc = new TXMLDocument(logConfig);

	xmlLogger = GetLogger(XMLDoc);
	Connect();
	urlWS = xmlLogger->Get_Config()->Get_DB()->Get_ConnectionString();
	machineID = String(xmlLogger->Get_Config()->Get_MachineID());

}

void TKLogWS::connErrors(void){

}

void TKLogWS::Connect(void){

	if(Connection!=NULL)
		delete Connection;

	Connection = new TIdHTTP(0);
	Connection->Request->Accept = "application/json";
	Connection->Request->ContentType = "application/json";
//	Connection->Request->Connection = "Keep-Alive";
	Connection->Request->CustomHeaders->AddValue("X-Token", "4xZsge5r");
//	Connection->Request->CustomHeaders->AddPair("X-Token", "4xZsge5r");

}

TKLogWS::~TKLogWS(void){

   delete Connection;

}

void TKLogWS::Log(TPLCFrame *f){

	logFrame = f;
	//Search Frame id
	for(int i=0;i<xmlLogger->Get_FramesLog()->Count;i++){
		if(f->idFrame == xmlLogger->Get_FramesLog()->Get_Frame(i)->Get_Id()){
			//Get all records to insert
			for(int j=0;j<xmlLogger->Get_FramesLog()->Get_Frame(i)->Count;j++){
			  xmlRecord = xmlLogger->Get_FramesLog()->Get_Frame(i)->Get_Record(j);
			  InsertRecord();
			}
		}

	}

}


void TKLogWS::Log(AnsiString applier, AnsiString src, AnsiString dst, AnsiString usr, AnsiString mid, AnsiString mtype, AnsiString desc, AnsiString v){


	int response;
	String temp;
	TJSONObject *JSObj = new TJSONObject();
	TStringStream * payloadStream;

	try{
		try{

			JSObj-> AddPair ("FrameCode", new TJSONNumber(0));
			JSObj-> AddPair ("Application", new TJSONString(applier));
			JSObj-> AddPair ("MachineID", new TJSONString(machineID));
			JSObj-> AddPair ("SourceAddress", new TJSONString(src));
			JSObj-> AddPair ("DestinationAddress", new TJSONString(dst));
			JSObj-> AddPair ("ExecutedByUser", new TJSONString(usr));
			JSObj-> AddPair ("ActionCode", new TJSONNumber(mid.ToInt()));
			JSObj-> AddPair ("ActionType", new TJSONString(mtype));
			JSObj-> AddPair ("Value", new TJSONNumber(v.ToInt()));
			JSObj-> AddPair ("Description", new TJSONString(desc));

			temp = JSObj->ToString();
			payloadStream = new TStringStream(temp,  TEncoding::UTF8, true);
			Connection->Post(urlWS,payloadStream);

//			response = Connection->ResponseCode;

		}catch(EIdSocketError &e){
			Connect();
			Connection->Post(urlWS,payloadStream);
		}
	}
	__finally{
		delete payloadStream;
		delete JSObj;
	}

}
void TKLogWS::InsertRecord(void){

	_di_IXMLFieldType field;
	int v;
	int response;
	String temp;

	TJSONObject *JSObj = new TJSONObject();
	TStringStream * payloadStream;
	 try{
		try{
			JSObj-> AddPair ("FrameCode", new TJSONNumber(logFrame->idFrame));
			//Get fields from record
			for(int i=0;i<xmlRecord->Get_Field()->Count;i++){
				field = xmlRecord->Get_Field()->Get_Item(i);
				if(field->Get_Type()=="field"){
					v = logFrame->getFrameDataValue(field->Get_fidx());
					JSObj-> AddPair (field->Get_Field(), new TJSONNumber(v));
				}else if(field->Get_Type()=="config"){
					JSObj-> AddPair (field->Get_Field(), new TJSONString(xmlLogger->Get_Config()->ChildNodes->FindNode(field->Get_Field())->Text));
				}else if(field->Get_Type()=="value"){
					JSObj-> AddPair (field->Get_Field(), new TJSONNumber(field->Get_Value()));
				}
			}

			temp = JSObj->ToString();
			payloadStream = new TStringStream(temp,  TEncoding::UTF8, true);
			Connection->Post(urlWS,payloadStream);
//			response = Connection->ResponseCode;

		}catch(EIdSocketError &e){
			Connect();
			Connection->Post(urlWS,payloadStream);
		}
	}
	__finally{
		delete payloadStream;
		delete JSObj;
	}


}




