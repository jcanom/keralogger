#ifndef TPLCFrameH
#define TPLCFrameH
//---------------------------------------------------------------------------

#include <vcl.h>
//#include "TObserverHdlr.h"
//#include "TKLogWrapper.h"

typedef void (__closure *ptrWorkDone)(TObject *Sender, int value);


class TFrameData{

	private:

	public:
		int idx;
		int bSize;
		int bit;
		int value;
		byte *pRead;
		AnsiString desc;
		AnsiString pcode;

};


class TPLCFrame {

   private:

		byte *cFrame;
		TList *lFData;
		int fSize;
		int rPeriod;
		int xmlSize;
		bool canParse;
		TMemoryStream *xmlStream;
		byte *sendBuff;
		AnsiString xmlLine;
		int aux1,aux2,aux3,aux4;
        int leak;
		void ExtractValues(void);


   public:

	  __fastcall TPLCFrame(int id);
	  __fastcall ~TPLCFrame(void);

	  int idFrame;

	  AnsiString ParseIT(byte *pFrame,AnsiString source);
	  void AddData(int fidx,int size,AnsiString pdesc,AnsiString pcode,int pbitNum);
	  int getFrameDataValue(int fdataidx);
	  int getFDCount(void);
	  void copyFrom(TPLCFrame *f);
//	  TPLCFrame *clone(void);
      void setLeakable(int leak);
};

#endif
