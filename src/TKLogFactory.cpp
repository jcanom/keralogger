//---------------------------------------------------------------------------
#pragma hdrstop

#include "TKLogFactory.h"
#include "TKLogger.h"
#include "TKLogFile.h"
#include "TKLogDB.h"
#include "TKLogWS.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)

TKLogger* TKLogFactory::getLogger(logger_type type) {

	TKLogger *logger = NULL;

	switch(type)
	{
		case LOG_FILE:
			logger = new TKLogFile();
			break;
		case LOG_DB:
		   logger = new TKLogDB();
		   break;
		case LOG_WS:
		   logger = new TKLogWS();
		   break;
		default:
		   logger = NULL;
		   logger = NULL;
		   break;
	}

	return logger;

}




