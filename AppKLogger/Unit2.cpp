//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit2.h"
#include "../DLL/KERALogger_DLL.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#pragma link "../DLL/Win32/Debug/KERALogger.lib"

TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm2::FormCreate(TObject *Sender)
{

	logSystem = new TKLogWrapper();
    KLG_Setup();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button1Click(TObject *Sender)
{
	logSystem->Log("APP_KLOGGER",Edit1->Text);

}
//---------------------------------------------------------------------------

void __fastcall TForm2::FormClose(TObject *Sender, TCloseAction &Action)
{
	KLG_Close();
}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button2Click(TObject *Sender)
{
	AnsiString applier = "APP_KLOGGER";
	AnsiString TXT = Edit1->Text;
	KLG_Log(applier.c_str(),TXT.c_str());
}
//---------------------------------------------------------------------------

