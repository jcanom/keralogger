
#ifndef KERALogger_DLL
#define KERALogger_DLL


__declspec(dllexport) void KLG_Setup(void);
__declspec(dllexport) void KLG_Close(void);
__declspec(dllexport) void KLG_Log(TPLCFrame *f);
__declspec(dllexport) void KLG_Log(char *applier, char *msg);
__declspec(dllexport) void KLG_Log(char *applier, char *src, char *dst, char *usr, char *mid, char *mtype, char *desc,char *value);
__declspec(dllexport) void KLG_Log(char *applier, int almS, int almT, int almI, int almD);
__declspec(dllexport) void KLG_Log(char *applier, int eventID);
__declspec(dllexport) void KLG_SetLanguage(int languageID);


#endif
