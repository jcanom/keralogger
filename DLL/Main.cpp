#include <vcl.h>
#include <windows.h>
#include "TKLogWrapper.h"
#include "KFrameLogger.h"

#pragma hdrstop
#pragma argsused

TKLogWrapper *logSystem;
KFrameLogger *frameLogger;

int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	return 1;
}


__declspec(dllexport) void KLG_Setup(void){

	logSystem = new TKLogWrapper();
	frameLogger = new KFrameLogger(logSystem);

}

__declspec(dllexport) void KLG_Close(void){

    delete frameLogger;
	delete logSystem;

}
// LOG FRAMES DEFINED IN FRAMEDEF
__declspec(dllexport) void KLG_Log(TPLCFrame *f){

	frameLogger->Log(f);

}
// LOG MESSAGES
__declspec(dllexport) void KLG_Log(char *applier, char *msg){

	logSystem->Log(applier, msg);

}
// LOG ACTIONS
__declspec(dllexport) void KLG_Log(char *applier, char *src, char *dst, char *usr, char *mid, char *mtype, char *desc,char *value){

	logSystem->Log(applier, src, dst, usr, mid, mtype, desc, value);

}
// LOG ALARMS
__declspec(dllexport) void KLG_Log(char *applier, int almS, int almT, int almI, int almD){

	logSystem->Log(applier, almS, almT, almI, almD);

}
// LOG MACHINE/SOFTWARE EVENTS
__declspec(dllexport) void KLG_Log(char *applier, int eventID){

	logSystem->Log(applier, eventID);

}
// CHANGE LOG LANGUAGE ASMessages.sib
__declspec(dllexport) void KLG_SetLanguage(int languageID){

	logSystem->SetLanguage(languageID);

}

